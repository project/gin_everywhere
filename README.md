# Gin Everywhere

CONTENTS OF THIS FILE
------------

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


INTRODUCTION
------------

This module enables Gin’s edit form layout for every content entity.

Besides the node form which is already covered by Gin, Term forms, Media forms,
User forms and many more will make usage of Gin's content form enhancements -
even your custom entity's forms.

It works by implementing Gin's hook_gin_content_form_routes_alter hook and
adding the routes for every content entity. It also makes sure that the
requirements of Gin's templates form structure is set up correctly for the
enabled forms.


REQUIREMENTS
------------

* Requires [Gin Admin Theme](https://www.drupal.org/project/gin)


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.
  See [installing modules](https://www.drupal.org/node/1897420) for further
  information.


CONFIGURATION
-------------

There is no configuration needed.


MAINTAINERS
-----------

Current maintainers:

* Pascal Crott - https://www.drupal.org/u/hydra
